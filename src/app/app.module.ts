import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule, List} from 'ionic-angular';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StatusBar} from '@ionic-native/status-bar';

import {MyApp} from './app.component';
import {HomePage} from '../pages/home/home';
import {HttpClientModule} from "@angular/common/http";
import {IonicImageViewerModule} from "ionic-img-viewer";
import {Geolocation} from "@ionic-native/geolocation";
import {DetalhesPage} from "../pages/detalhes/detalhes";
import {ListPage} from "../pages/list/list";
import {RouterPage} from "../pages/router/router";
import {Facebook} from "@ionic-native/facebook";
import {AvaliarPage} from "../pages/avaliar/avaliar";
import {Ionic2RatingModule} from "ionic2-rating";
import {Avaliar2Page} from "../pages/avaliar2/avaliar2";
import {BancoProvider} from '../providers/banco/banco';
import {FavoritosPage} from "../pages/favoritos/favoritos";
import {SQLite} from "@ionic-native/sqlite";
import {AngularFireModule} from "angularfire2";
import {GooglePlus} from "@ionic-native/google-plus";
import {AngularFireAuth} from "angularfire2/auth";
import {CadastroPage} from "../pages/cadastro/cadastro";

const firebaseConfig = {
    apiKey: "AIzaSyANYCfl4Yuht81ZtnnNctMdw8alanDo3uA",
    authDomain: "transporteescolar-3eb08.firebaseapp.com",
    databaseURL: "https://transporteescolar-3eb08.firebaseio.com",
    projectId: "transporteescolar-3eb08",
    storageBucket: "transporteescolar-3eb08.appspot.com",
    messagingSenderId: "914108243305"
};

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        DetalhesPage,
        ListPage,
        RouterPage,
        AvaliarPage,
        Avaliar2Page,
        FavoritosPage,
        CadastroPage
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        IonicImageViewerModule,
        AngularFireModule.initializeApp(firebaseConfig),
        Ionic2RatingModule,
        IonicModule.forRoot(MyApp)
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        DetalhesPage,
        ListPage,
        RouterPage,
        AvaliarPage,
        Avaliar2Page,
        FavoritosPage,
        CadastroPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Geolocation,
        Facebook,
        SQLite,
        GooglePlus,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        BancoProvider,
        AngularFireAuth
    ]
})
export class AppModule {
}
