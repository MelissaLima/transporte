import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, LoadingController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {HomePage} from '../pages/home/home';
import {RouterPage} from "../pages/router/router";
import {AvaliarPage} from "../pages/avaliar/avaliar";
import {FavoritosPage} from "../pages/favoritos/favoritos";
import {BancoProvider} from "../providers/banco/banco";
import {CadastroPage} from "../pages/cadastro/cadastro";
import {GooglePlus} from "@ionic-native/google-plus";
import {FacebookLoginResponse, Facebook} from "@ionic-native/facebook";
import {AngularFireAuth} from "angularfire2/auth";


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    // rootPage: any = CadastroPage;


    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public load: LoadingController, public auth: AngularFireAuth, public banco: BancoProvider, public plus: GooglePlus, public fb: Facebook) {
        platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();

            // this.banco.apagar();

            this.banco.consultarTabelaUser().then(data => {
                // console.log(data);
                if (data.length > 0) {
                    this.nav.setRoot(HomePage);
                } else {
                    this.nav.setRoot(RouterPage);
                }
            }, err => {
                this.nav.setRoot(RouterPage);
            });

        });

    }

    abreLogin() {
        this.nav.setRoot(RouterPage);
    }

    abreAvaliar() {
        this.nav.setRoot(AvaliarPage);
    }

    abreFavoritos() {
        this.nav.setRoot(FavoritosPage);
    }

    abreHome() {
        this.nav.setRoot(HomePage);
    }

    sair() {

        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        this.banco.consultarTabelaUser().then(data => {
            if (data[0].tipo == 2) {

                this.plus.trySilentLogin({}).then(data => {
                    this.plus.logout()
                        .then(res => {
                            console.log(res);
                            loading.dismiss();
                            this.banco.dropUser();
                            this.nav.setRoot(RouterPage);
                        })
                        .catch(err => {
                            console.error(err);
                            loading.dismiss();
                        });
                })
                    .catch(err => {
                        console.error(err);
                        let load = this.load.create({
                            spinner: 'hide',
                            content: 'Sem conexão!',
                            duration: 2000
                        });
                        loading.dismiss();
                        load.present();
                    });

            }

            if (data[0].tipo == 1) {

                this.fb.logout()
                    .then((res: FacebookLoginResponse) => {
                        console.log('Logout Facebook!', res);
                        loading.dismiss();
                        this.banco.dropUser();
                        this.nav.setRoot(RouterPage);
                    })
                    .catch(e => {
                        console.log('Error logout Facebook', e);
                        let load = this.load.create({
                            spinner: 'hide',
                            content: 'Sem conexão!',
                            duration: 2000
                        });
                        loading.dismiss();
                        load.present();
                    });

            }

            if (data[0].tipo == 0) {

                loading.dismiss();
                this.banco.dropUser();
                this.nav.setRoot(RouterPage);

            }
        })


    }
}

