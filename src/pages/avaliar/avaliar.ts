import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {Avaliar2Page} from "../avaliar2/avaliar2";

/**
 * Generated class for the AvaliarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-avaliar',
    templateUrl: 'avaliar.html',
})
export class AvaliarPage {

    tipo = "0";
    carros;
    motoristas;

    constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public load: LoadingController) {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();

        this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=carrosCidade&cidade=Fortaleza').subscribe(data => {
            if (data.length > 0) {
                this.carros = data;
            } else {
                this.carros = null;
            }

            this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=motoristasCidade&cidade=Fortaleza').subscribe(data => {
                if (data.length > 0) {
                    this.motoristas = data;
                } else {
                    this.motoristas = null;
                }

                loading.dismiss();
            }, err => {
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Sem conexão!',
                    duration: 2000
                });
                loading.dismiss();
                load.present();
            });

        }, err => {
            let load = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
                duration: 2000
            });
            loading.dismiss();
            load.present();
        });


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad AvaliarPage');
    }

    avaliar(p, n) {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();

        this.navCtrl.push(Avaliar2Page, {objeto: p, page: n});

        loading.dismiss();
    }

    teste() {
        console.log(this.tipo);
        console.log(this.motoristas);
    }
}
