import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Avaliar2Page } from './avaliar2';

@NgModule({
  declarations: [
    Avaliar2Page,
  ],
  imports: [
    IonicPageModule.forChild(Avaliar2Page),
  ],
})
export class Avaliar2PageModule {}
