import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {BancoProvider} from "../../providers/banco/banco";

/**
 * Generated class for the Avaliar2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-avaliar2',
    templateUrl: 'avaliar2.html',
})
export class Avaliar2Page {

    objeto;
    page;
    rate;
    obs = null;
    user;

    constructor(public banco: BancoProvider, public load: LoadingController, public navCtrl: NavController, public navParams: NavParams, public http: HttpClient) {
        this.objeto = this.navParams.get('objeto');
        this.page = this.navParams.get('page');


        this.banco.consultarTabelaUser().then(data => {
            this.user = data;
        })


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad Avaliar2Page');
    }

    onModelChange() {

    }

    avaliarBus() {

        if(this.rate > 0){
            let loading = this.load.create({
                spinner: 'dots',
                content: 'Aguarde...'
            });

            loading.present();

            if(!this.obs){
                this.obs = null;
            }

            this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=inserirAvBus&user=' + this.user[0].cod + '&bus=' + this.objeto[0].id + '&obs=' + this.obs + '&star=' + this.rate).subscribe(data => {
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Avaliação concluida com sucesso!',
                    duration: 3000
                });
                loading.dismiss();
                this.navCtrl.pop();
                load.present();
            }, err => {
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Sem conexão!',
                    duration: 2000
                });
                loading.dismiss();
                load.present();
            });
        }else{
            let load = this.load.create({
                spinner: 'hide',
                content: 'A avaliação por estrelas é obrigatória!',
                duration: 2000
            });
            load.present();
        }


    }

    avaliarMot() {

        if(this.rate > 0){
            let loading = this.load.create({
                spinner: 'dots',
                content: 'Aguarde...'
            });

            loading.present();

            this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=inserirAvMotorista&user=' + this.user[0].cod + '&mot=' + this.objeto.id + '&obs=' + this.obs + '&star=' + this.rate).subscribe(data => {
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Avaliação concluida com sucesso!',
                    duration: 3000
                });
                loading.dismiss();
                this.navCtrl.pop();
                load.present();
            }, err => {
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Sem conexão!',
                    duration: 2000
                });
                loading.dismiss();
                load.present();
            });
        }else{
            let load = this.load.create({
                spinner: 'hide',
                content: 'A avaliação por estrelar é obrigatória!',
                duration: 2000
            });
            load.present();
        }


    }

}
