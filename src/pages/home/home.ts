import {Component} from '@angular/core';
import {NavController, LoadingController, AlertController, MenuController} from 'ionic-angular';
import {HttpClient} from "@angular/common/http";
import {ListPage} from "../list/list";
import {Geolocation} from '@ionic-native/geolocation';
import set = Reflect.set;
import {BancoProvider} from "../../providers/banco/banco";
import {GooglePlus} from "@ionic-native/google-plus";

declare var google;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    latitude;
    longitude;
    map;
    paradas;
    locations: Array<{lat, lng}>;
    cidade;
    data;


    constructor(public plus: GooglePlus, public banco: BancoProvider, public menu: MenuController, public navCtrl: NavController, public http: HttpClient, public geo: Geolocation, public load: LoadingController, public alert: AlertController) {


    }

    ionViewDidLoad() {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Carregando mapa'
        });

        loading.present();

        this.geo.getCurrentPosition().then((resp) => {
            this.latitude = resp.coords.latitude;
            this.longitude = resp.coords.longitude;
            // this.latitude = -4.990780;
            // this.longitude = -38.318249;
            this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + this.latitude + ',' + this.longitude + '&key=AIzaSyDALIAqE4PUYVY2EFTqClHkaVCc8er9UzA').subscribe(data => {
                this.data = data;
                for (let i = 0; i < this.data['results'].length; i++) {
                    if (data['results'][i]['types'][0] == 'administrative_area_level_2') {
                        this.cidade = data['results'][i]['address_components'][0]['long_name'];
                    }
                }
                this.loadMap();
            }, err => {
                let loading = this.load.create({
                    spinner: 'hide',
                    content: 'Sem conexão!',
                });

                loading.present();

                setTimeout(() => {
                    loading.dismiss();
                }, 2000);

            });

            loading.dismiss();
        }).catch((error) => {
            console.log('Error getting location', error);
            loading.dismiss();
        });

    }

    loadMap() {

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: {lat: this.latitude, lng: this.longitude},
            mapTypeId: google.maps.MapTypeId.RODMAP
        }, err => {
            let loading = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
            });

            loading.present();

            setTimeout(() => {
                loading.dismiss();
            }, 2000);

        });


        this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=paradas&cidade=' + this.cidade).subscribe(data => {
            this.paradas = data;
            if (this.paradas.length > 0) {
                this.locations = [];
                var image = 'http://gtcontroller.com.br/api_rastreio/bus.png';

                for (var i = 0; i < this.paradas.length; i++) {
                    this.locations.push({
                        lat: this.paradas[i].latitude,
                        lng: this.paradas[i].logitude
                    })
                }


                var marker = [];
                for (var j = 0; j < this.locations.length; j++) {
                    var lat = this.locations[j].lat;
                    var lng = this.locations[j].lng;
                    var parada = this.paradas[j].id;
                    marker[j] = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, lng),
                        map: map,
                        icon: image
                    });
                    this.callMarker(j, lat, lng, parada, marker, this.paradas[j].formatted_address);

                }
            } else {
                let loading = this.load.create({
                    spinner: 'hide',
                    content: 'Nenhuma parada encontrada na sua região!',
                });

                loading.present();

                setTimeout(() => {
                    loading.dismiss();
                }, 2000);
            }

        }, err => {
            let loading = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
            });

            loading.present();

            setTimeout(() => {
                loading.dismiss();
            }, 2000);

        });


    }

    // loadMap2() {
    //     let mapOptions: GoogleMapOptions = {
    //         camera: {
    //             target: {
    //                 lat: 43.0741904,
    //                 lng: -89.3809802
    //             },
    //             zoom: 18,
    //             tilt: 30
    //         }
    //     };
    //
    //     this.map = GoogleMaps.create('map', mapOptions);
    //
    //     // Wait the MAP_READY before using any methods.
    //     this.map.one(GoogleMapsEvent.MAP_READY)
    //         .then(() => {
    //             console.log('Map is ready!');
    //
    //             // Now you can use all methods safely.
    //             this.map.addMarker({
    //                 title: 'Ionic',
    //                 icon: 'http://gtcontroller.com.br/api/appcontroller/placa.png',
    //                 animation: 'DROP',
    //                 position: {
    //                     lat: 43.0741904,
    //                     lng: -89.3809802
    //                 }
    //             })
    //                 .then(marker => {
    //                     marker.on(GoogleMapsEvent.MARKER_CLICK)
    //                         .subscribe(() => {
    //                             this.abreList();
    //                         });
    //                 });
    //
    //         });
    //
    // }


    callMarker(j, lat, lng, parada, marker, adress) {
        google.maps.event.addListener(marker[j], 'click', () => {
            let loading = this.load.create({
                spinner: 'dots'
            });

            this.navCtrl.push(ListPage, {lat: lat, lng: lng, parada: parada, adress: adress});

            loading.dismiss();
        });
    }


}

