import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {RouterPage} from "../router/router";

/**
 * Generated class for the DetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
    selector: 'page-detalhes',
    templateUrl: 'detalhes.html',
})
export class DetalhesPage {

    carro;
    motorista;
    onibus;
    time = new Date().getTime();
    latitude;
    longitude;

    constructor(public navCtrl: NavController, public navParams: NavParams, public load: LoadingController) {
        this.carro = this.navParams.get('carro');
        this.motorista = 'http://gtcontroller.com.br/api_rastreio/files/motoristas/' + this.carro[0].idmotorista + '.jpg';
        this.onibus = 'http://gtcontroller.com.br/api_rastreio/files/onibus/' + this.carro[0].deviceid + '.jpg';
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad DetalhesPage');

        this.latitude = this.navParams.get('lat');
        this.longitude = this.navParams.get('lng');
        this.carro = this.navParams.get('carro');

        let coord = new google.maps.LatLng(this.latitude, this.longitude);
        let coordbus = new google.maps.LatLng(this.carro[0].latitude, this.carro[0].longitude);


        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;


        var map2 = new google.maps.Map(document.getElementById('map2'), {
            zoom: 4,
            center: coordbus,
            mapTypeId: google.maps.MapTypeId.RODMAP
        }, err => {
            let loading = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
            });

            loading.present();

            setTimeout(() => {
                loading.dismiss();
            }, 2000);

        });

        directionsDisplay.setMap(map2);
        directionsDisplay.setOptions({suppressMarkers: true});

        directionsService.route({
            origin: coordbus,
            destination: coord,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });

        var marker = [];
        for (var j = 0; j < 2; j++) {
            marker[j] = new google.maps.Marker({
                position: coordbus,
                map: map2,
                icon: 'http://gtcontroller.com.br/api_rastreio/busescolar.png'
            });

        }

        var marker2 = [];
        for (var j = 0; j < 2; j++) {
            marker[j] = new google.maps.Marker({
                position: coord,
                map: map2,
                icon: 'http://gtcontroller.com.br/api_rastreio/bus.png'
            });

        }
    }




}
