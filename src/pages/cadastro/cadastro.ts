import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, MenuController, LoadingController} from 'ionic-angular';
import {BancoProvider} from "../../providers/banco/banco";
import {HomePage} from "../home/home";
import {HttpClient} from "@angular/common/http";

/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-cadastro',
    templateUrl: 'cadastro.html',
})
export class CadastroPage {

    email;
    data;
    endereco;
    instituicao;
    seg: any = false;
    ter: any = false;
    qua: any = false;
    qui: any = false;
    sex: any = false;
    senha;
    tipo;


    constructor(public load: LoadingController, public http: HttpClient, public navCtrl: NavController, public navParams: NavParams, public menu: MenuController, public banco: BancoProvider) {
        this.email = this.navParams.get('email');
        this.tipo = this.navParams.get('tipo');
        this.senha = this.navParams.get('senha');
    }

    ionViewDidLoad() {
        this.menu.enable(false);
        console.log('ionViewDidLoad CadastroPage');
    }

    salvar() {

        let loading = this.load.create({
            spinner: 'dots',
            content: 'Carregando mapa'
        });

        loading.present();

        if (this.seg == true) {
            this.seg = 1;
        } else {
            this.seg = 0;
        }

        if (this.ter == true) {
            this.ter = 1;
        } else {
            this.ter = 0;
        }

        if (this.qua == true) {
            this.qua = 1;
        } else {
            this.qua = 0;
        }

        if (this.qui == true) {
            this.qui = 1;
        } else {
            this.qui = 0;
        }

        if (this.sex == true) {
            this.sex = 1;
        } else {
            this.sex = 0;
        }

        if (this.tipo != 0) {
            this.senha = 0;
        }

        this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=inserirUser&email=' + this.email + '&dtn=' + this.data + '&senha=' + this.senha + '&instituicao=' + this.instituicao + '&endereco=' + this.endereco + '&seg=' + this.seg + '&ter=' + this.ter + '&qua=' + this.qua + '&qui=' + this.qui + '&sex=' + this.sex).subscribe(data => {
            this.banco.inserirUser(data, this.email, this.tipo, this.data, this.senha, this.instituicao, this.endereco, this.seg, this.ter, this.qua, this.qui, this.sex);
            this.menu.enable(true);
            this.navCtrl.setRoot(HomePage);
            loading.dismiss();

        }, err => {
            let load = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
                duration: 2000
            });
            loading.dismiss();
            load.present();
        });







    }

}
