import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import {BancoProvider} from "../../providers/banco/banco";
import {ListPage} from "../list/list";

/**
 * Generated class for the FavoritosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-favoritos',
    templateUrl: 'favoritos.html',
})
export class FavoritosPage {

    parada;

    constructor(public navCtrl: NavController, public navParams: NavParams, public banco: BancoProvider, public load: LoadingController) {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();

        this.banco.consultarTabela().then(data => {
            if (data.length > 0) {
                this.parada = data;
            }

            loading.dismiss();
        })
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad FavoritosPage');
    }

    abreList(p){
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();

        this.navCtrl.push(ListPage, {lat: p.latitude, lng: p.longitude, parada: p.cod_parada, adress: p.parada_nome});

        loading.dismiss();
    }

}
