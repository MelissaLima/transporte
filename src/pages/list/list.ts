import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {HomePage} from "../home/home";
import {HttpClient} from "@angular/common/http";
import {DetalhesPage} from "../detalhes/detalhes";
import {BancoProvider} from "../../providers/banco/banco";

declare var google;

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {

    carros;
    latitude;
    longitude;
    parada;
    datas;
    like = 0;
    adress;

    constructor(public banco: BancoProvider, public navCtrl: NavController, public navParams: NavParams, public http: HttpClient, public load: LoadingController) {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Buscando veículos'
        });

        loading.present();

        this.latitude = this.navParams.get('lat');
        this.longitude = this.navParams.get('lng');
        this.parada = this.navParams.get('parada');
        this.adress = this.navParams.get('adress');

        this.banco.consultarTabela2(this.parada).then(data => {
            if (data.length > 0) {
                this.like = 1;
            }
        })

        setTimeout(() => {
            loading.dismiss();
        }, 2000);

    }

    ionViewDidLoad() {
        this.banco.criar();

        this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=paradaId&id=' + this.parada + '&latitude=' + this.latitude + '&longitude=' + this.longitude).subscribe(data => {
            this.datas = data;
            if (this.datas.length > 0) {
                this.carros = data;
            } else {
                let loading = this.load.create({
                    spinner: 'hide',
                    content: 'Nenhum veículo programado para esta parada!',
                });

                loading.present();

                setTimeout(() => {
                    loading.dismiss();
                    this.navCtrl.pop();
                }, 2000);
            }

        }, err => {
            let loading = this.load.create({
                spinner: 'hide',
                content: 'Sem conexão!',
            });

            loading.present();

            setTimeout(() => {
                loading.dismiss();
                this.navCtrl.pop();
            }, 2000);

        });
    }

    doRefresh(refresher) {

        setTimeout(() => {
            this.http.get('http://gtcontroller.com.br/api_rastreio/index.php?a=paradaId&id=' + this.parada + '&latitude=' + this.latitude + '&longitude=' + this.longitude).subscribe(data => {
                if (data) {
                    this.carros = data;
                } else {
                    let loading = this.load.create({
                        spinner: 'hide',
                        content: 'Nenhum veículo programado para esta parada!',
                    });

                    loading.present();

                    setTimeout(() => {
                        loading.dismiss();
                        this.navCtrl.pop();
                    }, 2000);
                }

            }, err => {
                let loading = this.load.create({
                    spinner: 'hide',
                    content: 'Sem conexão!',
                });

                loading.present();

                setTimeout(() => {
                    loading.dismiss();
                    this.navCtrl.pop();
                }, 2000);

            });
            refresher.complete();
        }, 2000);
    }

    abreDetalhes(p) {
        let loading = this.load.create({
            spinner: 'dots'
        });

        this.navCtrl.push(DetalhesPage, {carro: p, lat: this.latitude, lng: this.longitude});

        loading.dismiss();
    }

    clickFavorito(){
        this.banco.inserir(this.parada, this.adress, this.latitude, this.longitude);
        this.like = 1;
    }

    clickUnFavorito(){
        this.banco.deleteParada(this.parada);
        this.like = 0;
    }
}
