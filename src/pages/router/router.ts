import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController, MenuController} from 'ionic-angular';
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook";
import {HttpClient} from "@angular/common/http";
import {GooglePlus} from "@ionic-native/google-plus";
import {AngularFireAuth} from "angularfire2/auth";
import {HomePage} from "../home/home";
import {BancoProvider} from "../../providers/banco/banco";
import {CadastroPage} from "../cadastro/cadastro";

/**
 * Generated class for the RouterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-router',
    templateUrl: 'router.html',
})

export class RouterPage {

    latitude;
    longitude;
    carros;
    displayName: any;
    email: any;
    senha;
    familyName: any;
    givenName: any;
    userId: any;
    imageUrl: any;

    isLoggedIn: boolean = false;


    constructor(public banco: BancoProvider, public menu: MenuController, public auth: AngularFireAuth, public plus: GooglePlus, public http: HttpClient, public navCtrl: NavController, public navParams: NavParams, public load: LoadingController, public fb: Facebook) {


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad RouterPage');
        this.menu.enable(false);
    }

    logar() {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();

        this.fb.login(['public_profile', 'email'])
            .then((res: FacebookLoginResponse) => {
                console.log('Logged into Facebook!', res);
                this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=buscaUser&email=' + res.authResponse.userID).subscribe(data => {

                    if (data.length > 0) {
                        this.menu.enable(true);
                        this.navCtrl.setRoot(HomePage);
                        this.banco.criarUser();
                        this.banco.inserirUser(data[0].cod_user, res.authResponse.userID, 1, data[0].dtn, data[0].senha, data[0].instituicao, data[0].endereco, data[0].seg, data[0].ter, data[0].qua, data[0].qui, data[0].sex);
                        loading.dismiss();
                    } else {
                        this.banco.criarUser();
                        this.navCtrl.setRoot(CadastroPage, {email: res.authResponse.userID, tipo: 1});
                        loading.dismiss();
                    }
                }, err => {
                    let load = this.load.create({
                        spinner: 'hide',
                        content: 'Sem conexão!',
                        duration: 2000
                    });
                    loading.dismiss();
                    load.present();
                });


            })
            .catch(e => {
                console.log('Error logging into Facebook', e);
                loading.dismiss();
            });
    }

    logarGoogle() {
        let loading = this.load.create({
            spinner: 'dots',
            content: 'Aguarde...'
        });

        loading.present();


        this.plus.login({})
            .then(res => {
                this.email = res.email;

                this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=buscaUser&email=' + this.email).subscribe(data => {
                    if (data.length > 0) {
                        this.banco.criarUser();
                        this.banco.inserirUser(data[0].cod_user, this.email, 2, data[0].dtn, data[0].senha, data[0].instituicao, data[0].endereco, data[0].seg, data[0].ter, data[0].qua, data[0].qui, data[0].sex);
                        this.menu.enable(true);
                        this.navCtrl.setRoot(HomePage);
                        loading.dismiss();
                    } else {
                        this.banco.criarUser();
                        this.navCtrl.setRoot(CadastroPage, {email: this.email, tipo: 2});
                        loading.dismiss();
                    }
                }, err => {
                    let load = this.load.create({
                        spinner: 'hide',
                        content: 'Sem conexão!',
                        duration: 2000
                    });
                    loading.dismiss();
                    load.present();
                });

            })
            .catch(err => {
                loading.dismiss();
                console.error(err)
            });


    }

    logarEmail() {
        if(this.email){
            if(this.senha){
                let loading = this.load.create({
                    spinner: 'dots',
                    content: 'Aguarde...'
                });

                loading.present();


                this.http.get('https://atendimento.gtcontroller.com.br/api_rastreio/index.php?a=buscaUser&email=' + this.email).subscribe(data => {

                    if (data.length > 0) {
                        if(this.senha == data[0].senha){
                            this.menu.enable(true);
                            this.navCtrl.setRoot(HomePage);
                            this.banco.criarUser();
                            this.banco.inserirUser(data[0].cod_user, this.email, 0, data[0].dtn, data[0].senha, data[0].instituicao, data[0].endereco, data[0].seg, data[0].ter, data[0].qua, data[0].qui, data[0].sex);
                            loading.dismiss();
                        }else{
                            loading.dismiss();
                            let load = this.load.create({
                                spinner: 'hide',
                                content: 'Senha não confere!',
                                duration: 2000
                            });

                            load.present();
                        }

                    } else {
                        this.banco.criarUser();
                        this.navCtrl.setRoot(CadastroPage, {email: this.email, tipo: 0, senha: this.senha});
                        loading.dismiss();
                    }

                }, err => {
                    let load = this.load.create({
                        spinner: 'hide',
                        content: 'Sem conexão!',
                        duration: 2000
                    });
                    loading.dismiss();
                    load.present();
                });
            }else{
                let load = this.load.create({
                    spinner: 'hide',
                    content: 'Preencha o campo de senha!',
                    duration: 2000
                });
                load.present();
            }

        }else{
            let load = this.load.create({
                spinner: 'hide',
                content: 'Preencha o campo de login!',
                duration: 2000
            });
            load.present();
        }


    }


}
