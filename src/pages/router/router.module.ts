import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouterPage } from './router';

@NgModule({
  declarations: [
    RouterPage,
  ],
  imports: [
    IonicPageModule.forChild(RouterPage),
  ],
})
export class RouterPageModule {}
