import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SQLite, SQLiteObject} from "@ionic-native/sqlite";
import {AngularFireAuth} from "angularfire2/auth";
import {GooglePlus} from "@ionic-native/google-plus";

/*
 Generated class for the BancoProvider provider.

 See https://angular.io/guide/dependency-injection for more info on providers
 and Angular DI.
 */
@Injectable()
export class BancoProvider {

    paradas: Array<Object>;

    constructor(public http: HttpClient, public sqlite: SQLite, public auth: AngularFireAuth, public plus: GooglePlus) {
        console.log('Hello BancoProvider Provider');
    }

    criar() {
        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql('CREATE TABLE IF NOT EXISTS paradas(cod_paradas INTEGER PRIMARY KEY, cod_parada INTEGER, parada_nome VARCHAR(50), latitude INTEGER, longitude INTEGER, email VARCHAR(50), tipo INTEGER )', {}).then(() => console.log('Executed SQL CREATE PARADA'))
                .catch(e => console.log(e));


        })
            .catch(e => console.log(e));
    }

    inserir(parada, nome, latitude, longitude) {

        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql("INSERT INTO paradas(cod_parada, parada_nome, latitude, longitude) VALUES (?, ?, ?, ?)", [parada, nome, latitude, longitude]).then((data) => {
                console.log("INSERTED PARADA: " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR INSERT PARADA: " + JSON.stringify(error.err));
            });


        })
            .catch(e => console.log(e));


    }

    apagar() {
        this.sqlite.deleteDatabase({
            name: 'data2',
            location: 'default'
        })

        console.log('SQL DELETE BASE')
    }

    drop() {

        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql("DROP TABLE IF EXISTS paradas", []).then((data) => {
                console.log("DROP PARADA': " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR DROP PARADA': " + JSON.stringify(error.err));
            });


        })
            .catch(e => console.log(e));


    }

    consultarTabela() {

        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: 'data2',
                location: 'default'
            }).then((db: SQLiteObject) => {


                db.executeSql("SELECT * FROM paradas ORDER BY cod_paradas DESC LIMIT 10", []).then((data) => {
                    this.paradas = [];
                    if (data.rows.length > 0) {
                        for (var i = 0; i < data.rows.length; i++) {
                            this.paradas.push({
                                cod_paradas: data.rows.item(i).cod_paradas,
                                cod_parada: data.rows.item(i).cod_parada,
                                parada_nome: data.rows.item(i).parada_nome,
                                latitude: data.rows.item(i).latitude,
                                longitude: data.rows.item(i).longitude

                            });
                        }

                        resolve(this.paradas);
                    } else {
                        resolve(0);
                    }
                }, (error) => {
                    console.log("ERROR SELECT PARADAS: " + JSON.stringify(error));
                    resolve(0);
                });


            }).catch(e => console.log(e));

            console.log('SQL SELECT PARADAS');
        });

    }

    deleteParada(parada) {
        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql("DELETE FROM paradas WHERE cod_parada = ?", [parada]).then((data) => {
                console.log("DELETE PARADA: " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR DELETE PARADA: " + JSON.stringify(error.err));
            });


        })
            .catch(e => console.log(e));
    }

    consultarTabela2(parada) {

        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: 'data2',
                location: 'default'
            }).then((db: SQLiteObject) => {


                db.executeSql("SELECT * FROM paradas WHERE cod_parada = ?", [parada]).then((data) => {
                    this.paradas = [];
                    if (data.rows.length > 0) {
                        for (var i = 0; i < data.rows.length; i++) {
                            this.paradas.push({
                                cod_paradas: data.rows.item(i).cod_paradas,
                                cod_parada: data.rows.item(i).cod_parada,
                                parada_nome: data.rows.item(i).parada_nome,
                                latitude: data.rows.item(i).latitude,
                                longitude: data.rows.item(i).longitude,
                                email: data.rows.item(i).email

                            });
                        }

                        resolve(this.paradas);
                    } else {
                        resolve(0);
                    }
                }, (error) => {
                    console.log("ERROR SELECT PARADAS: " + JSON.stringify(error));
                });


            }).catch(e => console.log(e));

            console.log('SQL SELECT PARADAS');
        });

    }


    criarUser() {
        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql('CREATE TABLE IF NOT EXISTS user(id INTEGER PRIMARY KEY, cod INTEGER, email VARCHAR(50), tipo INTEGER, dtn DATETIME, senha VARCHAR(50), instituicao VARCHAR(50), endereco VARCHAR(50), seg INTEGER, ter INTEGER, qua INTEGER, qui INTEGER, sex INTEGER)', {}).then(() => console.log('Executed SQL CREATE USER'))
                .catch(e => console.log(e));


        })
            .catch(e => console.log(e));
    }

    inserirUser(id, email, tipo, dtn, senha, instituicao, end, seg, ter, qua, qui, sex) {

        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql("INSERT INTO user(email, tipo, dtn, instituicao, endereco, seg, ter, qua, qui, sex, cod, senha) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [email, tipo, dtn, instituicao, end, seg, ter, qua, qui, sex, id, senha]).then((data) => {
                console.log("INSERTED USER: " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR INSERT USER: " + JSON.stringify(error.err));
            });


        })
            .catch(e => console.log(e));


    }

    consultarTabelaUser() {

        return new Promise((resolve, reject) => {
            this.sqlite.create({
                name: 'data2',
                location: 'default'
            }).then((db: SQLiteObject) => {


                db.executeSql("SELECT * FROM user", []).then((data) => {
                    this.paradas = [];
                    if (data.rows.length > 0) {
                        for (var i = 0; i < data.rows.length; i++) {
                            this.paradas.push({
                                email: data.rows.item(i).email,
                                tipo: data.rows.item(i).tipo,
                                dtn: data.rows.item(i).dtn,
                                endereco: data.rows.item(i).endereco,
                                senha: data.rows.item(i).senha,
                                seg: data.rows.item(i).seg,
                                ter: data.rows.item(i).ter,
                                qua: data.rows.item(i).qua,
                                qui: data.rows.item(i).qui,
                                sex: data.rows.item(i).sex,
                                instituicao: data.rows.item(i).instituicao,
                                cod: data.rows.item(i).cod,

                            });
                        }

                        resolve(this.paradas);
                    } else {
                        resolve(0);
                    }
                }, (error) => {
                    console.log("ERROR SELECT PARADAS: " + JSON.stringify(error));
                    resolve(0);
                });


            }).catch(e => console.log(e));

            console.log('SQL SELECT PARADAS');
        });

    }

    dropUser() {

        this.sqlite.create({
            name: 'data2',
            location: 'default'
        }).then((db: SQLiteObject) => {


            db.executeSql("DROP TABLE IF EXISTS user", []).then((data) => {
                console.log("DROP USER': " + JSON.stringify(data));
            }, (error) => {
                console.log("ERROR DROP USER': " + JSON.stringify(error.err));
            });


        })
            .catch(e => console.log(e));


    }


}
